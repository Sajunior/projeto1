import { Component } from '@angular/core';
import { NavController, IonicPage } from 'ionic-angular';
import firebase from 'firebase';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  loginForm: FormGroup
  afAuth: any;
  storage: any;

  constructor(public navCtrl: NavController,
    public formbuilder: FormBuilder) {

    this.loginForm = formbuilder.group({
      email:["", Validators.compose([Validators.required, Validators.email])],
      password:["", Validators.compose([Validators.required, Validators.minLength(6)])],
    })
  }
  submitLogin(){
    this.afAuth.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password)
    .then((response) =>{
      this.storage.set('user', response.user.uid )
      .then(() => {
      this.navCtrl.setRoot('start-page')
      })
    })
    .catch((error) =>{
      if(error.code == 'auth/wrong-password'){
        this.presentAlert('erro', 'Senha incorreta, Digite Novamente.');
        this.loginForm.controls['password'].setValue(null);
      }
    })

  }
  presentAlert(arg0: string, arg1: string): any {
    throw new Error("Method not implemented.");
  }

}
