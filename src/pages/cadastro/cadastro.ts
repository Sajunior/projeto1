import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HomePage } from '../home/home';
import { AngularFireAuth } from '@angular/fire/auth';

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  registerForm: FormGroup;
  loginForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public NavParams: NavParams,
    public formbuilder: FormBuilder,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController
  ){

    let ValidateConfirmPassword = (control: FormControl) => {
      if( control.value !== '' ){
        if(control.value === this.registerForm.controls.password.value){
          console.log(control.value === this.registerForm.controls.password.value)
          return null
        } else {
          console.log('invalid')
          return { "invalid": true }
        }
      }
    }
      
      this.registerForm = formbuilder.group({

      name: ['', Validators.compose ([Validators.required, Validators.minLength(5)])],
      email: ['', Validators.compose ([Validators.required, Validators.email])],
      password: ['', Validators.compose ([Validators.required, Validators.minLength(6)])],
      confirmPassword: ['', Validators.compose ([Validators.required, Validators.minLength(6), ValidateConfirmPassword])]
    })


  }
  submitForm(){ 
  this.afAuth.auth.createUserWithEmailAndPassword(
    this.registerForm.value.email, this.registerForm.value.password)
    .then ((Response) => {
      this.presentAlert('Usuario Cadastrado', 'Usuario Cadastrado com suecesso');
      this.navCtrl.setRoot('HomePage')
    })
    .catch((error) => {
      if(error.code == 'auth/email-already-in-use'){
        this.presentAlert('error', 'Email ja Cadastrado')
      }
    })
  }
  presentAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }  
}
